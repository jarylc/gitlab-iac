package utils

import (
	"io/ioutil"
	"os"
	"strings"
)

func IsIgnored(projectDir string, attr string) bool {
	ignoreFile := projectDir + "/.terraignore"
	if _, err := os.Stat(ignoreFile); err != nil {
		if os.IsNotExist(err) {
			return false
		}
	}
	dat, err := ioutil.ReadFile(ignoreFile)
	if err != nil {
		return false
	}
	return strings.Contains(string(dat), attr)
}
