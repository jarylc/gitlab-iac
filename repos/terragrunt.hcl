locals {
  owner = get_env("PROJECT_OWNER")
  iac   = get_env("CI_PROJECT_ID")
  user  = get_env("GITLAB_USER_LOGIN")
  token = get_env("GITLAB_USER_TOKEN")
}

remote_state {
  backend = "http"
  generate = {
    path      = "state.tf"
    if_exists = "overwrite_terragrunt"
  }
  config = {
    address  = "https://gitlab.com/api/v4/projects/${local.iac}/terraform/state/${path_relative_to_include()}.tfstate"
    username = "${local.user}"
    password = "${local.token}"
  }
}

generate "provider" {
  path      = "provider.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<-EOF
    terraform {
      required_providers {
        gitlab = {
          source  = "gitlabhq/gitlab"
          version = "~> 16.2.0"
        }
      }
    }
    provider "gitlab" {
      token = "${local.token}"
    }
  EOF
}

terraform {
  before_hook "before_echo" {
    commands = ["apply", "plan"]
    execute  = ["echo", "=> REPO: ${path_relative_to_include()}"]
  }
  after_hook "after_cleanup" {
    commands = ["apply"]
    execute  = ["rm", "-f", "${get_terragrunt_dir()}/final.yaml"]
  }
}

inputs = {
  owner = local.owner
}
